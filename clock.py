#!/usr/bin/env python3
import datetime as dt
import os
import shutil
import time

import delegator

import clock_config


def generate_time_commands(time: dt.datetime):
    hour = time.hour
    minute = time.minute

    hour_two_digits = str(hour).zfill(2)
    min_two_digits = str(minute).zfill(2)

    commands = """
        /silent-command remote.call("clock", "set", {hr_1}, {hr_2}, {min_1}, {min_2}, 
        {{hr_1=global.hr_1, hr_2=global.hr_2, min_1=global.min_1, min_2=global.min_2, reset=global.reset}})
    """.format(hr_1=hour_two_digits[0],
               hr_2=hour_two_digits[1],
               min_1=min_two_digits[0],
               min_2=min_two_digits[1])

    return commands


def prepare_run_directory(path, include_server_config):
    data_path = clock_config.data_path
    base_config = os.path.dirname(__file__) + "/config.ini"
    save_path = os.path.dirname(__file__) + "/trainclock.zip"
    mods_path = os.path.dirname(__file__) + "/mods"
    base_server_settings = os.path.dirname(__file__) + "/server-settings.json"

    server_folder = path

    if os.path.exists(server_folder):
        shutil.rmtree(server_folder)
    os.makedirs(server_folder)

    use_config = server_folder + "/config.ini"
    use_write = server_folder + "/write"
    use_server_config = server_folder + "/server-settings.json"
    saves = use_write + "/saves"
    mods = use_write + "/mods"

    os.makedirs(saves)

    shutil.copyfile(save_path, saves + "/trainclock.zip")
    shutil.copytree(mods_path, mods)

    shutil.copyfile(os.path.dirname(__file__) + "/player-data.json", use_write + "/player-data.json")

    with open(base_config, "r") as f:
        config_data = f.read()

    config_data = config_data.replace('read-data=__PATH__system-read-data__', 'read-data={}'.format(data_path))
    config_data = config_data.replace('write-data=__PATH__system-write-data__', 'write-data={}'.format(use_write))

    with open(use_config, "w") as f:
        f.write(config_data)

    if include_server_config:
        with open(base_server_settings, 'r') as f:
            server_data = f.read()

        server_data = server_data.replace('"public": true,', '"public": false,')
        server_data = server_data.replace('"name": "Name of the game as it will appear in the game listing",',
                                          '"name": "clock",')
        server_data = server_data.replace('"require_user_verification": true,', '"require_user_verification": false,')
        server_data = server_data.replace('"autosave_interval": 10,', '"autosave_interval": 1000000,')

        with open(use_server_config, 'w') as f:
            f.write(server_data)


def start_server():
    global server
    server_folder = "/tmp/serv"
    prepare_run_directory(server_folder, True)

    binary = clock_config.binary

    server = delegator.run([binary, '-c', server_folder + "/config.ini", '--server-settings', server_folder + "/server-settings.json",
                            '--start-server', server_folder + "/write/saves/trainclock.zip"], block=False)
    print('Server started')


def start_client():
    global client
    client_folder = "/tmp/client"
    prepare_run_directory(client_folder, False)

    binary = clock_config.binary
    client = delegator.run([binary, '-c', client_folder + "/config.ini", '--mp-connect', '127.0.0.1', '--fullscreen', '--disable-audio'], block=False)
    print('Client started')


def send_time_to_server():
    server.send(generate_time_commands(dt.datetime.now()).replace("\n", "    "))


start_server()

time.sleep(5)

speed = 4
server.send('/c game.speed = {}'.format(speed))
server.send('/c game.surfaces["nauvis"].wind_speed = game.surfaces["nauvis"].wind_speed / {}'.format(speed))

start_client()

while 1:
    time.sleep(1)
    send_time_to_server()

# print(generate_time_commands(dt.datetime.now()))
